//
//  EntrezResult.h
//  BioCocoa
//
//  Created by Alexander Griekspoor
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import <Cocoa/Cocoa.h>

@interface EntrezResult : NSObject <NSCoding>
{
    //===========================================================================
    //  Variables and properties
    //===========================================================================
    
    NSString* accession;
    NSString* description;
    NSString* species;
    NSString* extra;

    int db_id;

}

//===========================================================================
//  Init & Dealloc
//===========================================================================

- (id)initWithID: (int)value;
- (id)initWithCoder: (NSCoder*) coder;
- (void)dealloc;

//===========================================================================
//  Archiving and Unarchiving
//===========================================================================

- (void)encodeWithCoder: (NSCoder *) coder;

//===========================================================================
//  Accessor methods
//===========================================================================

- (NSString *)accession;
- (void)setAccession:(NSString *)newAccession;

- (NSString *)description;
- (void)setDescription:(NSString *)newDescription;

- (NSString *)species;
- (void)setSpecies:(NSString *)newSpecies;

- (int)db_id;
- (void)setDb_id:(int)newDb_id;

- (NSString *)extra;
- (void)setExtra:(NSString *)newExtra;


//===========================================================================
//  General methods
//===========================================================================


//===========================================================================
//  Sorting
//===========================================================================

- (NSComparisonResult)sortResultsOnIdAscending: (EntrezResult*) aResult;
- (NSComparisonResult)sortResultsOnIdDescending:(EntrezResult*) aResult;

- (NSComparisonResult)sortResultsOnAccessionAscending: (EntrezResult*) aResult;
- (NSComparisonResult)sortResultsOnAccessionDescending:(EntrezResult*) aResult;

- (NSComparisonResult)sortResultsOnDescriptionAscending: (EntrezResult*) aResult;
- (NSComparisonResult)sortResultsOnDescriptionDescending:(EntrezResult*) aResult;

- (NSComparisonResult)sortResultsOnSpeciesAscending: (EntrezResult*) aResult;
- (NSComparisonResult)sortResultsOnSpeciesDescending:(EntrezResult*) aResult;

@end
