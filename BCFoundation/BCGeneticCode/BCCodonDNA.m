//
//  BCCodon.m
//  BioCocoa
//
//  Created by John Timmer on 8/31/04.
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCCodonDNA.h"
#import "BCSequence.h"
#import "BCAminoAcid.h"
#import "BCSymbol.h"
#import "BCNucleotideDNA.h"


static BCCodonDNA *unmatchedCodon = nil;



@implementation BCCodonDNA



- (BCCodonDNA *)initWithDNASequenceString: (NSString *)sequenceString andAminoAcidString: (NSString *)aaString {
    self = [super init];
    if ( self == nil )
        return self;
    
    if ( ![aaString isEqualToString: @"stop"] )
        codedAminoAcid = [BCAminoAcid performSelector: NSSelectorFromString( aaString )];
    else
        codedAminoAcid = nil;
    
    if ( [sequenceString length] != 3)
        return nil;
    
    firstBase = [BCNucleotideDNA symbolForChar: [sequenceString characterAtIndex: 0]];
    if ( firstBase == nil || firstBase == [BCNucleotideDNA undefined] )
        return nil;
    
    secondBase = [BCNucleotideDNA symbolForChar: [sequenceString characterAtIndex: 1]];
    if ( secondBase == nil || secondBase == [BCNucleotideDNA undefined] )
        return nil;
    
    wobbleBase = [BCNucleotideDNA symbolForChar: [sequenceString characterAtIndex: 2]];
    if ( wobbleBase == nil || wobbleBase == [BCNucleotideDNA undefined] )
        return nil;
    
    return self;
}


- (BCCodon *) init {
    return nil;
}

//- (void) dealloc {
//    
//    [super release];
//}



+ (BCCodonDNA *)unmatched {
    if ( unmatchedCodon == nil)
        unmatchedCodon = [[BCCodonDNA alloc] initWithDNASequenceString: @"---" andAminoAcidString: @"undefined"];
    return unmatchedCodon;
}


- (BOOL) matchesTriplet: (NSArray *)entry {
    if ( ![(BCNucleotideDNA *)[entry objectAtIndex: 0] isRepresentedBySymbol: (BCNucleotideDNA *)firstBase] )
        return NO;
    
    if ( ![(BCNucleotideDNA *)[entry objectAtIndex: 1] isRepresentedBySymbol: (BCNucleotideDNA *)secondBase] )
        return NO;
    
    if ( ![(BCNucleotideDNA *)[entry objectAtIndex: 2] isRepresentedBySymbol: (BCNucleotideDNA *)wobbleBase] )
        return NO;
    
    return YES;
}





- (BCSequence *) triplet {
    NSArray *tempArray = [NSArray arrayWithObjects: firstBase, secondBase, wobbleBase, nil];
    return [BCSequence sequenceWithSymbolArray: tempArray];
}


@end
