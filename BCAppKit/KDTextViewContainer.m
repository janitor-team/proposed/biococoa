//
//  KDTextViewContainer.m
//  LineNumbering
//
//  Created by Koen van der Drift on Sat May 01 2004.
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "KDTextViewContainer.h"


@implementation KDTextViewContainer

- (id) initWithContainerSize:(NSSize) size {
    self = [super initWithContainerSize:size];
	
	// default columnwidth (width of 10 characters in Courier 12)
    columnWidth = 90.0;

    return self;
}


- (BOOL) isSimpleRectangularTextContainer {
    return NO;
}

- (void) setColumnWidth:(float) width {
    columnWidth = width;
    [[self layoutManager] textContainerChangedGeometry:self];
}

- (float) columnWidth {
    return columnWidth;
}

- (NSRect)lineFragmentRectForProposedRect:(NSRect)proposedRect 
        sweepDirection:(NSLineSweepDirection)sweepDirection 
        movementDirection:(NSLineMovementDirection)movementDirection 
        remainingRect:(NSRect *)remainingRect
{
	
	if(proposedRect.origin.x <= 0.0)
		proposedRect.origin.x = kLEFT_MARGIN_WIDTH;
	
	proposedRect.size.width = columnWidth;
	
    if (proposedRect.origin.x + 2 * columnWidth - 20.0 >= [self containerSize].width) *remainingRect = NSZeroRect;
    else {
        remainingRect->origin.x = proposedRect.origin.x + columnWidth - 10.0;
        remainingRect->origin.y = proposedRect.origin.y;
        remainingRect->size.width = [self containerSize].width - proposedRect.origin.x - columnWidth;
        remainingRect->size.height = proposedRect.size.height;
    }
	
	
    return proposedRect;
}

@end
