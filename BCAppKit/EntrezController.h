//
//  EntrezController.h
//  BioCocoa
//
//  Created by Alexander Griekspoor
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import <AppKit/AppKit.h>
#import "EntrezResult.h"
#import <SXML/SXML.h>
#import <SystemConfiguration/SystemConfiguration.h>


@interface EntrezController : NSWindowController{

    //===========================================================================
    //  Outlets
    //===========================================================================
    
    IBOutlet NSSearchField *searchField;
    IBOutlet NSMenu *searchMenu;
    IBOutlet NSButton *fetchButton;
    IBOutlet NSButton *cancelButton;
    IBOutlet NSTableView *tv;
    IBOutlet NSTextView *preview;
    IBOutlet NSProgressIndicator *progress;
    IBOutlet NSTextField *progressTextField;
    
    //===========================================================================
    //  Variables
    //===========================================================================
    
    NSUserDefaults *prefs;

    NSMutableArray *searchResults;
    SXMLTree *hits;
    
    NSURLConnection *connection;
    NSMutableData *receivedData;
    NSURLResponse *response;
    long long bytesReceived;
    BOOL searchInProgress;
    BOOL summaryFetchInProgress;
    BOOL fetchInProgress;
    int searchcount;
    int fetchedResults;
    
    NSString *webenv;
    NSString *querykey;
    
    id delegate;
}



//===========================================================================
#pragma mark -
#pragma mark • Init & Dealloc
//===========================================================================

- (void)awakeFromNib;
- (void)dealloc;

- (void)setDelegate:(id)newDelegate;
- (id)delegate;

//===========================================================================
#pragma mark -
#pragma mark • ACTIONS
//===========================================================================

- (IBAction)searchForQuery:(id)sender;
- (IBAction)fetch:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)showPreview:(id)sender;


//===========================================================================
#pragma mark -
#pragma mark • TABLEVIEW METHODS
//===========================================================================

- (int)numberOfRowsInTableView:(NSTableView *)theTableView;
- (id)tableView:(NSTableView *)theTableView objectValueForTableColumn:(NSTableColumn *)theColumn row:(int)rowIndex;
- (void)tableView:(NSTableView *)aTableView willDisplayCell:(id)aCell forTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex;
- (void)tableViewSelectionDidChange:(NSNotification *)aNotification;
- (void)tableView:(NSTableView*)tv didClickTableColumn:(NSTableColumn *)tableColumn;
- (void)clearIndicatorImages;


// ================================================================
#pragma mark --- DOWNLOAD METHODS
// ================================================================

- (void)retrieveSearchResultsForQuery: (NSString *)q;
- (BOOL)parseSearchResults: (NSData *)results;

- (void)retrieveSummaries;
- (BOOL)parseSummaries: (NSData *)results;

- (void)fetchResult: (EntrezResult *)result;
- (BOOL)parseFetch: (NSData *)results;

- (void)cleanupDownload;

- (void)reportDownloadFailureWithError: (NSString *)errorstring;

- (void) NCBIconnectionError: (id) anError;

// ================================================================
#pragma mark --- DOWNLOAD ACCESSORS
// ================================================================

- (NSURLResponse *)response;
- (void)setResponse:(NSURLResponse *)newResponse;

- (NSString *)webenv;
- (void)setWebenv:(NSString *)newWebenv;

- (NSString *)querykey;
- (void)setQuerykey:(NSString *)newQuerykey;



// ================================================================
#pragma mark --- CONNECTION DELEGATES
// ================================================================

- (void)connection: (NSURLConnection *)theConnection didReceiveResponse: (NSURLResponse *)theresponse;
- (void)connection: (NSURLConnection *)connection didReceiveData: (NSData *)data;
- (void)connection: (NSURLConnection *)connection didFailWithError: (NSError *)error;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;


//===========================================================================
#pragma mark -
#pragma mark --- DELEGATES
//===========================================================================

- (BOOL)validateMenuItem:(NSMenuItem *)anItem;

//===========================================================================
#pragma mark -
#pragma mark --- GENERAL METHODS
//===========================================================================

- (BOOL)parseOutput: (NSData *)output;
- (BOOL) _canConnect;


@end
