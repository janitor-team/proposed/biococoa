//
//  TestBCSequenceProperties.m
//  BioCocoa
//
//  Created by Koen van der Drift on 4/5/05.
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "TestBCSequenceProperties.h"


@implementation TestBCSequenceProperties

- (void)testSequenceProperties
{
	BCSequence				*sequence;
	BCToolMassCalculator	*calculator;
	NSNumber				*expected, *obtained;
	float					accuracy;
	
	sequence = [BCSequence sequenceWithString:@"KPYTREDFRQWERTYIPLKHGFDSACVNM"];
	calculator = [BCToolMassCalculator massCalculatorWithSequence: sequence];
	accuracy = 0.05;
	
	[calculator setMassType: BCMonoisotopic];	
	expected = [NSNumber numberWithFloat: (3587.7316 - hydrogenMonoisotopicMass)];
	obtained = [[calculator calculateMass] objectAtIndex: 0];
	
	STAssertEqualsWithAccuracy([expected floatValue], [obtained floatValue], accuracy, @"The MW should be %f but is %f",
				   [expected floatValue], [obtained floatValue] );

	[calculator setMassType: BCAverage];	
	expected = [NSNumber numberWithFloat: (3590.1056 - hydrogenAverageMass)];
	obtained = [[calculator calculateMass] objectAtIndex: 0];
	
	STAssertEqualsWithAccuracy([expected floatValue], [obtained floatValue], accuracy, @"The MW should be %f but is %f",
				   [expected floatValue], [obtained floatValue] );
}


@end
